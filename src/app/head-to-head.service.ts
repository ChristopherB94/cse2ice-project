import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Fixture } from './fixture';

const httpOptionsA = { headers: new HttpHeaders ({ 'X-Auth-Token': 'e41a94078d35473fa3025f6efd766638'})
};


@Injectable({
  providedIn: 'root'
})
export class HeadToHeadService {

  constructor(private http: HttpClient) { }

  loadHeadToHead(id: string): Observable <Fixture[]> {
    var urlPrefix = "http://api.football-data.org/v1/fixtures/";

    return this.http.get<Fixture[]>(urlPrefix+id, httpOptionsA);
  }
}