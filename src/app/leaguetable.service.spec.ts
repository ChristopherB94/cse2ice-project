import { TestBed, inject } from '@angular/core/testing';

import { LeaguetableService } from './leaguetable.service';

describe('LeaguetableService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LeaguetableService]
    });
  });

  it('should be created', inject([LeaguetableService], (service: LeaguetableService) => {
    expect(service).toBeTruthy();
  }));
});
