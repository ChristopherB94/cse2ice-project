/** class to retrieve league table data */
export class LeagueTable {
    rank: number;
    team: string;
    teamId: number;
    playedGames: number;
    crestURI: string;
    points: number;
    goals: number;
    goalsAgainst: number;
    goalDifference: number;
}
