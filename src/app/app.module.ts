import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { LeaguetableComponent } from './leaguetable/leaguetable.component';
import { LeaguetableService } from './leaguetable.service';
import { FixturesComponent } from './fixtures/fixtures.component';
import { SoccerAppRoutingModule } from './/soccer-app-routing.module';
import { HeadToHeadComponent } from './head-to-head/head-to-head.component';
import { SeasonComponent } from './season/season.component';

@NgModule({
  declarations: [
    AppComponent,
    LeaguetableComponent,
    FixturesComponent,
    HeadToHeadComponent,
    SeasonComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    SoccerAppRoutingModule
  ],
  providers: [LeaguetableService],
  bootstrap: [AppComponent]
})
export class AppModule { }
