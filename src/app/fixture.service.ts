import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Fixture } from './fixture';

/** Request Header to access api data */
const httpOptionsA = { headers: new HttpHeaders ({ 'X-Auth-Token': 'e41a94078d35473fa3025f6efd766638'})
};

@Injectable({
  providedIn: 'root'
})
export class FixtureService {

  constructor(private http: HttpClient) { }
  
  /** Method to direct link to JSON for fixtures */
  loadFixtures(id: string): Observable <Fixture[]> {
    var urlPrefix = "http://api.football-data.org/v1/teams/";
    var urlSuffix = "/fixtures";

    return this.http.get<Fixture[]>(urlPrefix+id+urlSuffix, httpOptionsA);
  }
}
