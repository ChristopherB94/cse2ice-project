import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { LeagueTable } from './league-table'

/** request header for api */
const httpOptionsA = { headers: new HttpHeaders ({ 'X-Auth-Token': 'e41a94078d35473fa3025f6efd766638'})
};

@Injectable({
  providedIn: 'root'
})
export class LeaguetableService {

  constructor(private http: HttpClient) { }
  
 /** method to return url with desired id */
  loadLeagueTable(leagueID: string): Observable <LeagueTable[]>{
    var urlPrefix = "http://api.football-data.org/v1/competitions/";
    var urlSuffix = "/leagueTable";

    return this.http.get<LeagueTable[]>(urlPrefix+leagueID+urlSuffix, httpOptionsA);
  }

}
