/** Class to receive fixture data */
export class Fixture {
  public constructor(public date: string, public homeTeamName: string, public awayTeamName: string, public result: FixtureResult, head: FixtureHead2Head) {}
}
/** class to retrieve object of JSON fixtures>results */
export class FixtureResult {
  public constructor(public goalsHomeTeam: number, public goalsAwayTeam: number) {}
}
/** class to retrieve fixture information (not working) */
export class FixtureHead2Head { 
  public constructor(public homeTeamWins: number, public awayTeamWins: number, public draws: number) {}
}