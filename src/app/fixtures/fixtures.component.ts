import { Component, OnInit } from '@angular/core';
import { Fixture } from '../fixture';
import { FixtureService } from '../fixture.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-fixtures',
  templateUrl: './fixtures.component.html',
  styleUrls: ['./fixtures.component.css']
})
export class FixturesComponent implements OnInit {

  fixtures: Fixture[];

  constructor(private fixtureService: FixtureService) { }

  ngOnInit() {
    this.getFixtures()
  }

  getFixtures() {
    this.fixtureService.loadFixtures('65').subscribe(fixtures => this.fixtures = fixtures);
  }

}
