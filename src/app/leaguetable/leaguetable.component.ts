import { Component, OnInit } from '@angular/core';
import { LeagueTable } from '../league-table';
import { LeaguetableService } from '../leaguetable.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-leaguetable',
  templateUrl: './leaguetable.component.html',
  styleUrls: ['./leaguetable.component.css']
})
export class LeaguetableComponent implements OnInit {

  league: LeagueTable[];

  constructor(private leagueTableService: LeaguetableService) { }

  ngOnInit() {
    this.getLeagueTable();
  }

  getLeagueTable() {
    this.leagueTableService.loadLeagueTable('445').subscribe(league => this.league = league);
  }
}
