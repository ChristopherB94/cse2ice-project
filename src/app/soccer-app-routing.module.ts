import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LeaguetableComponent } from './leaguetable/leaguetable.component';
import { FixturesComponent } from './fixtures/fixtures.component';
import { HeadToHeadComponent } from './head-to-head/head-to-head.component';
import { SeasonComponent } from './season/season.component';

/** paths for the different pages */
const routes : Routes = [
  { path: 'leaguetable', component: LeaguetableComponent },
  { path: 'fixtures', component: FixturesComponent },
  { path: 'headtohead', component: HeadToHeadComponent }, 
  { path: 'season', component: SeasonComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class SoccerAppRoutingModule { }
