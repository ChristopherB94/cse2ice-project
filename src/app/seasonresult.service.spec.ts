import { TestBed, inject } from '@angular/core/testing';

import { SeasonresultService } from './seasonresult.service';

describe('SeasonresultService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SeasonresultService]
    });
  });

  it('should be created', inject([SeasonresultService], (service: SeasonresultService) => {
    expect(service).toBeTruthy();
  }));
});
