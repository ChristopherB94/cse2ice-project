import { Component, OnInit } from '@angular/core';
import { Fixture } from '../fixture';
import { HeadToHeadService } from '../head-to-head.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-head-to-head',
  templateUrl: './head-to-head.component.html',
  styleUrls: ['./head-to-head.component.css']
})
export class HeadToHeadComponent implements OnInit {

  head: Fixture[];

  constructor(private headToHeadService: HeadToHeadService) { }

  ngOnInit() {
    this.getHeadToHead()
  }

  getHeadToHead() {
    this.headToHeadService.loadHeadToHead('65').subscribe(head => this.head = head);
  }

}
