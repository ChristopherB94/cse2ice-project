import { Component, OnInit } from '@angular/core';
import { Fixture } from '../fixture';
import { SeasonService } from '../seasonresult.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-season',
  templateUrl: './season.component.html',
  styleUrls: ['./season.component.css']
})
export class SeasonComponent implements OnInit {

  season: Fixture[];

  constructor(private seasonService: SeasonService) { }

  ngOnInit() {
    this.getSeason()
  }

  getSeason() {
    this.seasonService.loadSeason('65').subscribe(season => this.season = season);
  }

}
