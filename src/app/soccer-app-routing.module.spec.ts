import { SoccerAppRoutingModule } from './soccer-app-routing.module';

describe('SoccerAppRoutingModule', () => {
  let soccerAppRoutingModule: SoccerAppRoutingModule;

  beforeEach(() => {
    soccerAppRoutingModule = new SoccerAppRoutingModule();
  });

  it('should create an instance', () => {
    expect(soccerAppRoutingModule).toBeTruthy();
  });
});
